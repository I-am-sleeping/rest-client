package by.training.rest.client;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import by.training.rest.model.Chapter;
import by.training.rest.model.Document;

public class RestClient {

    public static void main(String[] args) {
        Client client = ClientBuilder.newClient()
                .register(JacksonJaxbJsonProvider.class);
        WebTarget target = client.target("http://localhost:8080/rest/services");

        Document doc = target.path("documents/1").request().get(Document.class);
        System.out.println(doc);

        List<Document> docs = target.path("documents").request()
                .get(new GenericType<List<Document>>() {
                });
        for (Document d : docs) {
            System.out.println(d);
        }

        doc = new Document();
        doc.setName("NEW");
        List<Chapter> chapters = new ArrayList<>();
        Chapter chapter = new Chapter();
        chapter.setId(1);
        chapter.setNumber(1);
        chapter.setPages(1000);
        chapters.add(chapter);
        doc.setChapters(chapters);
        target.path("documents").request().post(Entity.xml(doc));
        System.out.println(
                target.path("documents/6").request().get(Document.class));

        doc.setName("Changed name");
        target.path("documents/6").request().put(Entity.xml(doc));
        chapter = new Chapter();
        chapter.setNumber(13);
        chapter.setPages(666);
        target.path("documents/6/chapters/1").request()
                .put(Entity.xml(chapter));
        System.out.println(
                target.path("documents/6").request().get(Document.class));

        System.out.println(target.path("documents/6/chapters").request()
                .delete().readEntity(String.class));

        target.path("documents/6").request().delete();
        System.out.println(target.request().get().getStatus());

    }

}
